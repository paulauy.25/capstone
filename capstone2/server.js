const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');

const usersRoutes = require('./routes/usersRoutes.js');

const productsRoutes = require('./routes/productsRoutes.js');

const ordersRoutes = require('./routes/ordersRoutes.js')

const port = 5000;

const app = express();

//Mongo DB connection

mongoose.connect("mongodb+srv://admin:admin@batch288uy.vm2f3w2.mongodb.net/AlingCellyStore?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	})

	let db = mongoose.connection


	db.on("error", console.error.bind(console, "Network problem, can't connect to the db!"))

	db.once("open", ()=>console.log('Connected to the database!'))



//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//Add Routing

app.use('/users',usersRoutes);
app.use('/products', productsRoutes);
app.use('/orders',ordersRoutes);



app.listen(port, () => console.log(`The server is running at port ${port}!`))