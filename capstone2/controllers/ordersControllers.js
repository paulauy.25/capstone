const express = require('express');
const Order = require('../models/Orders.js');
const User = require('../models/Users.js');
const auth = require('../auth');
const Product = require('../models/Products.js');

module.exports.createOrder = async (request, response) => {
  const userId = auth.decode(request.headers.authorization).id;
  const productId = request.params.productId;
  const quantity = request.body.quantity;

  try {
    const product = await Product.findById(productId);

    if (!product) {
      return response.status(404).send(false);
    }

    const totalAmount = product.price * quantity;

    const newOrder = new Order({
      userId: userId,
      products: [{ 
        productId: productId, 
        quantity: quantity,
        productName: product.name, 
        productDescription: product.description, 
      }],
      totalAmount: totalAmount,
    });

    await newOrder.save();

    response.send(true);
  } catch (error) {
    response.status(500).send(false);
  }
};

module.exports.retrieveUsers = (request, response) => {
  const userId = request.params.userId; 

  User.findById(userId)
    .then(user => {
      if (!user) {
        return response.status(404).send(false);
      }
      
      Order.find({ userId: userId })
        .populate('products.productId')
        .then(orders => {
          const userData = {
            id: user._id,
            username: user.username,
            email: user.email,
            orders: orders
          };

          return response.send(userData);
        })
        .catch(error => {
          return response.status(500).send(false);
        });
    })
    .catch(error => {
      return response.status(500).send(false);
    });
};
