const mongoose = require('mongoose')
const productsSchema = new mongoose.Schema({

  name: {
    type: String,
    required: [true, "Product name is required"]
  },

  description: {
    type: String,
    required: [true, "Product description is required"]
  },

  isAvailable: {
    type: Boolean,
    required: [true, "Product availability is required"]
  },

  createdOn: {
    type: Date,
    default: new Date()
  },

  stock: {
    type: Number,
    required: [true, "Amount of products in stock is required"]
  },

  price: {
    type: Number,
    required: [true, "Product price is required"]
  },

  owners: [
    {
      userId: {
        type: String,
        required: [true, "Owner ID is required"]
      },

      transactionDate:{
        type: Date,
        default: new Date()
      }
    }
  ]

})

const Products = mongoose.model("Product", productsSchema)
module.exports = Products