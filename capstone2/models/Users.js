const mongoose = require('mongoose')
const usersSchema = new mongoose.Schema({

  firstName: {
    type: String,
    required: [true, "First Name is required"]
  },

  lastName: {
    type: String,
    required: [true, "Last Name is required"]
  },

  email: {
    type: String,
    requried: [true, "Email is required"]
  },

  password: {
    type: String,
    requried: [true, "Password is required"]
  },

  isAdmin: {
    type: Boolean,
    default: false
  },

  mobileNo: {
    type: String,
    requried: [true, "Mobile No. is required"]
  },

  orders: [
    {
      productId: {
        type: String, 
        required: [true, "Product ID is required"]
      },

      orderedOn: {
        type: Date, 
        default: new Date()
      },

      status: {
        type: String, 
        default: "Ordered"
      }
    }
  ]
})

const User = mongoose.model("Users", usersSchema)
module.exports = User
