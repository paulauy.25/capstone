const express = require('express')

const router = express.Router()

const productsControllers = require('../controllers/productsControllers.js')
const auth = require('../auth.js')


//Routes


router.post('/createProduct', productsControllers.createProduct)
router.get('/retrieveAllProducts', productsControllers.retrieveAllProducts)
router.get('/retrieveActiveProducts', productsControllers.retrieveProducts)
router.get('/retrieveUnavailableProducts', productsControllers.retrieveUnavailableProducts)
router.get('/:productId', productsControllers.retrieveSingleProduct)
router.patch('/updateDescription', productsControllers.updateDescription)
router.patch('/deactivate', productsControllers.archiveProduct)
router.patch('/activate', productsControllers.unarchiveProduct)
router.patch('/updateStock', productsControllers.updateStock)
router.patch('/:productId/updatePrice', auth.verify, productsControllers.updatePrice)

module.exports = router