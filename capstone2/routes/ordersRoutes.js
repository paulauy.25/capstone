const express = require('express');
const ordersControllers = require('../controllers/ordersControllers');
const auth = require("../auth.js");
const router = express.Router();

// Routes
router.post('/:productId', auth.verify, ordersControllers.createOrder)
// router.get('/:userId', auth.verify, ordersControllers.retrieveUsers);
module.exports = router;
