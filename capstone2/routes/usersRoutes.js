const express = require('express')
const router = express.Router()
const usersControllers = require('../controllers/usersControllers.js')
const auth = require('../auth.js')


//Routes

router.post("/registerUser", usersControllers.registerUser)
router.post("/orderProduct", usersControllers.orderProduct)
router.get("/getAllUsers", usersControllers.getAllUsers)
router.get("/getUserOrders", auth.verify, usersControllers.getUserOrders)
router.post("/loginUser", usersControllers.loginUser)
router.get("/userDetails", auth.verify, usersControllers.retrieveUserDetails)
router.get("/:userId/retrieveUserOrders", auth.verify, usersControllers.retrieveUserOrders)
router.patch("/:userId/setAsAdmin", auth.verify, usersControllers.setAsAdmin)

module.exports = router