import { Container, Button, Row, Col, Card, Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Highlights() {
  return (
    <Container className="mt-2">

     <Row className="mt-1">
       <Col>
         <Carousel interval={2000}>
           <Carousel.Item>
             <img
               className="d-block w-100"
               src="https://assets3.thrillist.com/v1/image/3112455/1200x630/flatten;crop_down;webp=auto;jpeg_quality=70"
               alt="First slide"
               style={{ height: "320px", objectFit: "cover" }}
             />
           </Carousel.Item>
           <Carousel.Item>
             <img
               className="d-block w-100"
               src="https://persontage.com.pk/wp-content/uploads/2021/10/WhatsApp-Image-2021-10-14-at-18.20.19.jpeg"
               alt="Third slide"
               style={{ height: "320px", objectFit: "cover" }}
             />
           </Carousel.Item>
           <Carousel.Item>
             <img
               className="d-block w-100"
               src="https://hips.hearstapps.com/bpc.h-cdn.co/assets/16/06/1600x800/landscape-1455048673-pizza-snacks.jpg?resize=1200:*"
               alt="Second slide"
               style={{ height: "320px", objectFit: "cover" }}
             />
           </Carousel.Item>
           <Carousel.Item>
             <img
               className="d-block w-100"
               src="https://static.vecteezy.com/system/resources/thumbnails/020/033/977/small_2x/summer-sale-banner-template-promotion-with-product-3d-product-display-hello-summer-holiday-beach-horizontal-banner-hi-summer-vacation-discount-travel-poster-colorful-tropical-sea-beach-landscape-free-vector.jpg"
               alt="Third slide"
               style={{ height: "320px", objectFit: "cover" }}
             />
           </Carousel.Item>
         </Carousel>
       </Col>
     </Row>


      <Row>
     <Link to="/user" className="d-flex justify-content-center" style={{ textDecoration: 'none' }}>
       <Button
         variant="info"
         className="mt-3 col-6 text-center"
         style={{
           background: 'linear-gradient(to right, #ff6b6b, #56ab2f)',
           color: 'white',
           borderRadius: '20px',
           padding: '10px 20px',
           fontSize: '16px',
           fontWeight: 'bold',
           letterSpacing: '1px',
           transition: 'background 0.3s ease',
         }}
         onMouseOver={e => e.target.style.background = '#ff6b6b'}
         onMouseOut={e => e.target.style.background = 'linear-gradient(to right, #ff6b6b, #56ab2f)'}
       >
         Explore Products
       </Button>
     </Link>

        <Col className="col-12 col-md-4 mt-3">
          <Card className="cardHighlight" style={{ background: '#333', color: '#fff', borderRadius: '10px', boxShadow: '0 2px 6px rgba(0, 0, 0, 0.3)' }}>
            <Card.Body>
              <Card.Title style={{ fontSize: '24px', fontWeight: 'bold', marginBottom: '10px' }}>Learn From Home</Card.Title>
              <Card.Text style={{ fontSize: '16px' }}>
                Aling Celly is a dynamic and innovative modern store that offers a wide range of cutting-edge electronic devices, gadgets, snacks, and accessories. Our mission is to provide exceptional customer service and a seamless shopping experience.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>

        <Col className="col-12 col-md-4 mt-3">
          <Card className="cardHighlight" style={{ background: '#333', color: '#fff', borderRadius: '10px', boxShadow: '0 2px 6px rgba(0, 0, 0, 0.3)' }}>
            <Card.Body>
              <Card.Title style={{ fontSize: '24px', fontWeight: 'bold', marginBottom: '10px' }}>Buy Now, Pay Later</Card.Title>
              <Card.Text style={{ fontSize: '16px' }}>
                Embrace the freedom of shopping today and enjoy the convenience of paying later. Experience the joy of getting what you want now without worrying about immediate payment.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>

        <Col className="col-12 col-md-4 mt-3">
          <Card className="cardHighlight" style={{ background: '#333', color: '#fff', borderRadius: '10px', boxShadow: '0 2px 6px rgba(0, 0, 0, 0.3)' }}>
            <Card.Body>
              <Card.Title style={{ fontSize: '24px', fontWeight: 'bold', marginBottom: '10px' }}>Be part of our Community</Card.Title>
              <Card.Text style={{ fontSize: '16px' }}>
                Join us and be part of our thriving Store community, where innovation and passion unite.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>



      </Row>

  
    </Container>
  );
}
