import { Container, Row, Col } from 'react-bootstrap';

export default function Banner() {
  return (
   <Container>
     <Row>
       <Col className='mt-3 text-center'>
         <h1 style={{
           color: 'red',
           textShadow: '2px 2px 4px rgba(0, 0, 0, 0.5)',
           fontFamily: 'Arial, sans-serif',
           fontSize: '48px',
           letterSpacing: '2px',
           fontWeight: 'bold',
          
         }}>
           Aling Celly Store
         </h1>
       </Col>
     </Row>
   </Container>

  );
}
