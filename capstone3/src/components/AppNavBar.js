import { Container, Nav, Navbar } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { useState, useContext } from 'react';
import UserContext from '../UserContext.js';

export default function AppNavBar() {
  const { user, setUser } = useContext(UserContext);

  return (
    <>
   <Navbar className='navbar bg-warning d-md-flex'>
     <Container>
       <Navbar.Brand as={Link} to='/user'>
         <span className='text-white' style={{ fontSize: '24px', fontWeight: 'bold', letterSpacing: '2px' }}>Zuitt</span>
       </Navbar.Brand>
       <Nav className='ms-auto'>
         {user.isAdmin !== true ? (
           <Nav.Link as={Link} to='/' className='navlink text-white'>
             Home
           </Nav.Link>
         ) : (
           <>
             <Nav.Link as={Link} to='/adminPage' className='navlink text-white'>
               Admin Portal
             </Nav.Link>
             <Nav.Link as={Link} to='/createProducts' className='navlink text-white'>
               Create Product
             </Nav.Link>
             <Nav.Link as={Link} to='/products' className='navlink text-white'>
               Products
             </Nav.Link>
           </>
         )}

         {user.id === null && user.isAdmin !== true ? null : (
           <Nav.Link as={Link} to='/logout' className='navlink text-white'>
             Logout
           </Nav.Link>
         )}

         {user.id === null && user.isAdmin !== true ? (
           <>
             <Nav.Link as={Link} to='/register' className='navlink text-white'>
               Register
             </Nav.Link>
             <Nav.Link as={Link} to='/login' className='navlink text-white'>
               Login
             </Nav.Link>
           </>
         ) : null}
       </Nav>
     </Container>

   </Navbar>

    </>
  );
}
