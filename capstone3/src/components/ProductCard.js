import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {useState, useEffect, useContext} from 'react';
import {Link} from 'react-router-dom';

export default function ProductCard(props){

	const {user} = useContext(UserContext);
	const {_id, name, description, price, isActive} = props.productProp;
	const [count, setCount] = useState(0);
	const [stocks, setStocks] = useState(30);
	const [isDisabled, setIsDisabled] = useState(false);

	return(
		<Container>
			<Row>
				<Col className = 'col-10 mt-3 '>
					<Card className="text-center cardHighlight">
					      
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Text>
					          {description}
					        </Card.Text>
					        
					      </Card.Body>
					      <Card.Footer className="text-muted">
					     	 <Button as = {Link} to = {`/products/${_id}`}variant="primary">Check Details</Button>
					      </Card.Footer>

					</Card>
				</Col>
			</Row>
		</Container>
	)
}