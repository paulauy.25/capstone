import React, { useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function DisplayProducts(props) {
  const { user } = useContext(UserContext);

  const { _id,description, price, isActive, name } = props.UserProductProp;

  return (
    <Container>
      <Row>
        <Col className="mt-3 mx-auto">
          <table className="table">
            <thead>
              <tr>
                <th>Field</th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Name</td>
                <td style={{ wordWrap: 'break-word', maxWidth: '300px' }}>{name}</td>
              </tr>
              <tr>
                <td>Description</td>
                <td style={{ wordWrap: 'break-word', maxWidth: '300px' }}>{description}</td>
              </tr>
              <tr>
                <td>Price</td>
                <td>PHP {price}</td>
              </tr>
              <tr>
                <Button as={Link} to={`/products/${_id}`} className="mt-3" variant="success">Check Details</Button>
              </tr>

            </tbody>
          </table>
            <hr />
        </Col>
      </Row>
    </Container>

  );
}
