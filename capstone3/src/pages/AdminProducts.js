import { Container, Row, Col, Button, Card, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Swal2 from 'sweetalert2';

export default function AdminProducts(props) {
  const [isDisabled, setIsDisabled] = useState(true);
  const [updatedDescription, setUpdatedDescription] = useState('');
  const [updatedIsAvailable, setUpdatedIsAvailable] = useState(true);

  const { _id, name, description, isAvailable, createdOn, price } = props.productProp;

  useEffect(() => {
    if (updatedDescription !== '') {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [updatedDescription]);

  function updateDescription(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/updateDescription`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        productId: _id,
        description: updatedDescription,
      }),
    })
      .then((result) => result.json())
      .then((data) => {
        if (data) {
          Swal2.fire({
            title: 'Update Successful!',
            icon: 'success',
            text: 'Product description was successfully updated',
          }).then(() => {
            window.location.reload();
          });
        }
      });
  }

  function deactivateProduct(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/deactivate`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        productId: _id,
      }),
    })
      .then((result) => result.json())
      .then((data) => {
        if (data === true) {
          Swal2.fire({
            title: 'Item successfully deactivated!',
            icon: 'success',
            text: 'Happy Shopping',
          }).then(() => {
            window.location.reload();
          });
        } else {
          Swal2.fire({
            title: 'Item is already deactivated',
            icon: 'error',
            text: 'Please choose a different item',
          });
        }
      });
  }

  function activateProduct(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/activate`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        productId: _id,
      }),
    })
      .then((result) => result.json())
      .then((data) => {
        if (data === true) {
          Swal2.fire({
            title: 'Item Activated!',
            icon: 'success',
            text: 'Happy Shopping',
          }).then(() => {
            window.location.reload();
          });
        } else {
          Swal2.fire({
            title: 'Item is already activated',
            icon: 'error',
            text: 'Please choose a different item',
          });
        }
      });
  }

	return(
	<Container>
	  <Row className="d-flex flex-wrap justify-content-center">
	    <Col>
	      <Card>
	        <Card.Body>
	          <div className="d-flex flex-column">
	            <div>
	              <Card.Title>{name}</Card.Title>
	            </div>
	            <div>
	              <Card.Subtitle className="mt-3" style={{ fontWeight: 'bold' }}>
	                Description
	              </Card.Subtitle>
	              <Card.Text>{description}</Card.Text>
	            </div>
	            <div>
	              <Form onSubmit={event => updateDescription(event)}>
	                <Form.Group className="mb-3" controlId="formUpdateDescription">
	                  <Form.Label style={{ fontWeight: 'bold' }}>
	                    Update Description
	                  </Form.Label>
	                  <Form.Control
	                    type="description"
	                    placeholder="Input updated description"
	                    value={updatedDescription}
	                    onChange={event => setUpdatedDescription(event.target.value)}
	                  />
	                </Form.Group>
	                <div className="d-grid justify-content-start">
	                  <Button
	                    id="updateDescriptionButton"
	                    type="submit"
	                    className="text-start bg-success"
	                    disabled={isDisabled}
	                  >
	                    Update
	                  </Button>
	                </div>
	              </Form>
	            </div>
	            <div>
	              <Card.Subtitle className="mt-3 mb-2" style={{ fontWeight: 'bold' }}>
	                In Stock
	              </Card.Subtitle>
	              <Card.Text>{JSON.stringify(isAvailable)}</Card.Text>
	            </div>
	         <div className="d-flex justify-content-start">
	           <div>
	             <Button
	               id="activeButton"
	               type="archive"
	               className="dButton bg-danger me-2"
	               onClick={event => deactivateProduct(event)}
	             >
	               Deactivate
	             </Button>
	           </div>
	           <div>
	             <Button
	               id="deactiveButton"
	               type="archive"
	               className="bg-dark"
	               onClick={event => activateProduct(event)}
	             >
	               Activate
	             </Button>
	           </div>
	         </div>

	          </div>
	        </Card.Body>
	      </Card>
	    </Col>
	  </Row>
	</Container>


	)
}