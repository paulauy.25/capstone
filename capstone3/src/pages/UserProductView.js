import {useState, useEffect, useContext} from 'react'
import {Container,Table, Row, Col, Card, Button} from 'react-bootstrap'
import Swal2 from 'sweetalert2'
import {useNavigate, useParams} from 'react-router-dom'
import UserContext from '../UserContext.js'

export default function UserProductView() {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [stock, setStock] = useState('');
  const [totalItems, setTotalItems] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const [isPlusDisabled, setIsPlusDisabled] = useState(false);
  const [isMinusDisabled, setIsMinusDisabled] = useState(false);

  const { productId } = useParams();
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(result => result.json())
      .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setStock(data.stock);
      });
  }, []);

  useEffect(() => {
    if (stock === 0) {
      Swal2.fire({
        title: 'Out of Stock!',
        icon: 'info',
        text: 'Product is not available'
      });
      setIsPlusDisabled(true);
    } else {
      setIsPlusDisabled(false);
    }
  }, [stock]);

  useEffect(() => {
    if (totalItems === 0) {
      setIsMinusDisabled(true);
    } else {
      setIsMinusDisabled(false);
    }
  }, [totalItems]);

  const add = (event) => {
    event.preventDefault();
    setTotalItems(totalItems + 1);
    setTotalPrice(price * (totalItems + 1));
    setStock(stock - 1);
  };

  const subtract = (event) => {
    event.preventDefault();
    setTotalItems(totalItems - 1);
    setTotalPrice(price * (totalItems - 1));
    setStock(stock + 1);
  };

  const checkout = (event) => {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/orderProduct`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        id: productId
      })
    })
      .then(result => result.json())
      .then(data => {
        if (data === true) {
          Swal2.fire({
            title: 'Checkout Successful',
            icon: 'success',
            text: 'Thank you for ordering!'
          });
        } else {
          Swal2.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again'
          });
        }
      });

    fetch(`${process.env.REACT_APP_API_URL}/products/updateStock`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        productId: productId,
        stock: stock
      })
    })
      .then(result => result.json())
      .then(data => console.log(data));

    navigate('/user');
  };
	return (
		<>
		<Container className="col-6 mt-3">
		  <Row>
		    <Col>
		     <h1 className="text-center" style={{ color: 'blue', letterSpacing: '2px', textShadow: '2px 2px 4px rgba(0, 0, 0, 0.5)', fontSize: '48px', margin: '0', padding: '20px', background: 'linear-gradient(to right, #00b8ff, #3f51b5)', border: '2px solid #3f51b5', borderRadius: '10px', boxShadow: '0 0 10px rgba(0, 0, 0, 0.3)' }}>
		       Checkout Order
		     </h1>

		      <Card className="mx-auto" style={{ backgroundColor: '#f8f9fa', border: 'none' }}>
		        <Card.Body>
		          <Table responsive>
		            <tbody>
		              <tr>
		                <td className="font-weight-bold">Name</td>
		                <td>{name}</td>
		              </tr>
		              <tr>
		                <td className="font-weight-bold">Description</td>
		                <td>{description}</td>
		              </tr>
		              <tr>
		                <td className="font-weight-bold">Price</td>
		                <td>{price}</td>
		              </tr>
		              <tr>
		                <td className="font-weight-bold">Stock</td>
		                <td>{stock}</td>
		              </tr>
		            </tbody>
		          </Table>
		          <div className="m-2">
		            <Button variant="primary" onClick={() => add(event)} disabled={isPlusDisabled}>
		              +
		            </Button>
		            <Button
		              variant="danger"
		              className="ms-2"
		              onClick={() => subtract(event)}
		              disabled={isMinusDisabled}
		            >
		              -
		            </Button>
		            <div>
		              <p className="font-weight-bold mt-3" style={{ color: 'green' }}>Total Products: {totalItems}</p>
		              <p className="font-weight-bold" style={{ color: 'red' }}>Total Amount: {totalPrice}</p>
		            </div>
		          </div>
		          <Button variant="success" onClick={(event) => checkout(event)} style={{ width: '100%', fontWeight: 'bold', letterSpacing: '1px' }}>
		            Checkout
		          </Button>
		        </Card.Body>
		      </Card>
		    </Col>
		  </Row>
		</Container>



		</>
	)
}