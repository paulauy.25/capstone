import {Container, Row, Col, Button, Form} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Link,useNavigate, Navigate, useLocation} from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal2 from 'sweetalert2'

export default function Login () {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isDisabled, setIsDisabled] = useState(true)

    const{user, setUser} = useContext(UserContext)
    const navigate = useNavigate()
    const location = useLocation()

    useEffect(()=>{
        if (email !== '' && password !== ''){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }
    },[email, password])

    useEffect(()=>{
        if(location.pathname === '/login' && localStorage.getItem('token')){
            navigate('*')
        }       
    }, [location.pathname, navigate])

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    function login (event) {
        event.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/loginUser`, {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(result => result.json())
        .then(data => {
            if (data===false) {
                Swal2.fire({
                    title: 'User not registered!',
                    icon: 'info',
                    text: 'Please register to login.'
                })
            }else{
                localStorage.setItem('token', data.auth)
                retrieveUserDetails(data.auth)
                Swal2.fire({
                    title: 'Login Successful!',
                    icon: 'success',
                    text: 'Happy Shopping!'
                })
            }
        })
    }

    return (
        (user.id === null)
        ? 
        (
            <Container>
                <Row>
                    <Col className = 'mx-auto col-6 m-3'>
                        <Form onSubmit={event=> login(event)}>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email Address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    value = {email} 
                                    onChange = {event => setEmail(event.target.value)}
                                    placeholder="Enter email" 
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Password" 
                                    value = {password}
                                    onChange = {event => setPassword(event.target.value)}
                                />
                            </Form.Group>

                            <div className="d-grid">
                                <Button id = "loginButton" type="submit" className="text-center mx-auto" disabled = {isDisabled}>
                                Login
                                </Button>
                            </div>
                                <p className = "text-center">don’t have account yet? Register here <Link to = "/register">Register here</Link></p>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
        : 
        (
            (user.isAdmin === true)
            ? 
            <Navigate to="/adminPage"/>
            : 
            <Navigate to="/" />
        )
    )
}