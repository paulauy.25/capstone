import React, { useState, useEffect } from 'react';
import ProductsView from '../components/ProductsView';

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/retrieveAllProducts`)
      .then((result) => result.json())
      .then((data) => {
        setProducts(data);
      });
  }, []);

  return (
    <>
      <h1 className="text-center mt-3">Products</h1>
      {products.map((product) => (
        <ProductsView key={product._id} productProp={product} />
      ))}
    </>
  );
}
