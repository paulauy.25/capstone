import {Container, Row, Col, Form, InputGroup, Button, Dropdown} from 'react-bootstrap'
import React, {useState, useEffect} from 'react'
import {useNavigate} from 'react-router-dom'
import Swal2 from 'sweetalert2'

export default function CreateProduct() {
	
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [isActive, setIsActive] = useState(true)
	const [stock, setStock] = useState(1)
	const [price, setPrice] = useState(0)
	const [isDisabled, setIsDisabled] = useState(true)
	const navigate = useNavigate()

	useEffect(()=>{
		if (name !== '' && description !== '' && isActive !== '' && stock !== '' && price !== ''){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}
	},[name, description, isActive,stock, price])

	function createProduct (event) {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				isAvailable: isActive,
				stock: stock,
				price: price
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)
			if (data===true) {
				Swal2.fire({
					title: 'Product successfully registered!',
					icon: 'success',
					text: 'Happy selling!'
				}).then(() => {
					window.location.reload()
				})
				navigate('/products')
			}else{
				Swal2.fire({
					title: 'There was a problem in product creation',
					icon: 'error',
					text: 'Kindly recheck the product details'
				})
			}
		})

	}

	return(
		<>
		<h1 className = 'm-3 text-center'>Create a New Product</h1>
		<Container>
		  <Row>
		    <Col className="col-6 mx-auto">
		      <Form onSubmit={event => createProduct(event)}>
		        <Form.Group className="mb-3" controlId="formName">
		          <strong><Form.Label>Name</Form.Label></strong>
		          <Form.Control
		            type="name"
		            value={name}
		            onChange={event => setName(event.target.value)}
		            placeholder="Enter product name"
		          />
		        </Form.Group>

		        <Form.Group className="mb-3" controlId="formDescription">
		          <strong><Form.Label>Description</Form.Label></strong>
		          <Form.Control
		            type="description"
		            placeholder="Enter product description"
		            value={description}
		            onChange={event => setDescription(event.target.value)}
		          />
		        </Form.Group>

		        <Form.Group className="mb-3" controlId="formStock">
		          <strong><Form.Label>Stock</Form.Label></strong>
		          <Form.Control
		            type="stock"
		            placeholder="Enter product stock"
		            value={stock}
		            onChange={event => setStock(event.target.value)}
		          />
		        </Form.Group>

		        <Form.Group className="mb-3" controlId="formPrice">
		          <strong><Form.Label>Price</Form.Label></strong>
		          <Form.Control
		            type="price"
		            placeholder="Enter product price"
		            value={price}
		            onChange={event => setPrice(event.target.value)}
		          />
		        </Form.Group>

		        <div className="d-grid">
		          <Button
		            id="createProdButton"
		            type="submit"
		            className="text-center mx-auto bg-success"
		            disabled={isDisabled}
		          >
		            Create Product
		          </Button>
		        </div>
		      </Form>
		    </Col>
		  </Row>
		</Container>

		</>
	)
}