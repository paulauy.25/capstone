import { Container, Row, Col } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import DisplayProducts from '../components/DisplayProducts.js';
import UserContext from '../UserContext';


export default function UserCatalog() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/retrieveActiveProducts`)
      .then((result) => result.json())
      .then((data) => {
        console.log(data);
        setProducts(data);
      });
  }, []);

  return (
    <>
  <h1 className="m-3 text-center" style={{ color: 'purple', fontSize: '36px', textTransform: 'uppercase', letterSpacing: '2px', textShadow: '2px 2px 4px rgba(0, 0, 0, 0.5)' }}>Products Available</h1>
  <Container>
    <Row>
      <Col>
        <>
          {products.map((product) => (
            <DisplayProducts key={product._id} UserProductProp={product} />
          ))}
        </>
      </Col>
    </Row>
  </Container>

    </>
  );
}
