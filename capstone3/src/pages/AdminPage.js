import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import ProductsView from '../components/ProductsView';
import AdminProducts from '../pages/AdminProducts.js';



export default function AdminPage() {
  const navigate = useNavigate();
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/retrieveAllProducts`)
      .then(result => result.json())
      .then(data => {
        console.log(data);
        setProducts(data);
      });
  }, []);

  const CreateProduct = () => {
    navigate('/createProducts');
  };

  const Products = () => {
    navigate('/products');
  };

  return (
    <>
      <h2 className='text-center mt-4'>Admin Dashboard</h2>
      <Container className='text-center'>
        <Row className="mb-3">
          <Col className="col-6 d-flex justify-content-end">
            <Button variant = "success"  className='cretProd' onClick={CreateProduct}>
              Add New Product
            </Button>
          </Col>
          <Col className="col-6 d-flex">
            <Button variant="danger"  className='Prod' onClick={Products}>
              Show All Products
            </Button>
          </Col>
        </Row>
      </Container>
      {products.map((product) => (
        <AdminProducts key={product._id} productProp={product} />
      ))}
    </>
  );

}
