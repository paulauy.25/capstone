import './App.css';
import AppNavBar from './components/AppNavBar.js'
import Home from './pages/Home.js'
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Products from './pages/Products.js'
import Logout from './pages/Logout.js'
import NoPageFound from './pages/NoPageFound.js'
import AdminPage from './pages/AdminPage.js'
// import AdminProducts from './pages/AdminProducts.js'
import CreateProduct from './pages/CreateProducts.js';
import AdminProducts from './pages/AdminProducts.js'
import UserCatalog from './pages/UserCatalog';
import UserProductView from './pages/UserProductView';



import {useState, useEffect} from 'react'
import {BrowserRouter, Route, Routes} from 'react-router-dom'
import {UserProvider} from './UserContext.js'
import 'bootstrap/dist/css/bootstrap.min.css'


function App() {
  
  const [user, setUser] = useState ({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    if (!localStorage.getItem('token')) {
      unsetUser();
    }
  }, []);

  useEffect(()=>{
  if(localStorage.getItem('token')){
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
      setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
    })  
  }
},[])


  return (
    <div className="App">

      <UserProvider value = {{user, setUser, unsetUser}}>
        <BrowserRouter>
          <AppNavBar/>

            <Routes>
              <Route path = '/' element = {<Home/>}/>
              <Route path = '/register' element = {<Register/>}/>
              <Route path = '/login' element = {<Login/>}/>
              <Route path = '/products' element = {<Products/>}/>
              <Route path = '/logout' element = {<Logout/>}/>
              <Route path = '/adminPage' element = {<AdminPage/>} />
              {/*<Route path = '/adminProduct' element = {<AdminProducts/>} />*/}
              <Route path = '/createProducts' element = {<CreateProduct/>} />
              <Route path = '/adminProduct' element = {<AdminProducts/>} />
              <Route path = '/user' element ={<UserCatalog/>} />
               <Route path = '/products/:productId'element = {<UserProductView/>} />
              <Route path = '*' element = {<NoPageFound/>}/>

            </Routes>

        </BrowserRouter>
      </UserProvider>
    </div>
  );
}

export default App;
